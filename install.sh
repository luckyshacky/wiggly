#!/bin/env bash
test $2
if [ ! $? ]
then
	echo "missing arguments.."
	echo "example: ./install.sh /dev/sda --efi"
	echo "example: ./install.sh /dev/sda --bios"
	exit 1
fi
echo preparing installer...
pacman-key --init
pacman-key --populate
pacman -Sy
if [ ! $? ]
then
	echo "cannot connect to mirrors..."
	exit 1
fi
bootpkgs=(
	"linux"
	"linux-headers"
	"linux-firmware"
	"pulseaudio-jack"
	"sudo"
	"v4l2loopback-dkms"
	"fastfetch"
	"zip"
	"unzip"
	"xf86-video-amdgpu"
	"xf86-video-intel"
	"xf86-video-nouveau"
	"pulseaudio"
	"pulseaudio-alsa"
	"wireplumber"
	"alsa-firmware"
	"grub"
	"base"
	"mkinitcpio"
	"packagekit"
	"dhcpcd"
	"networkmanager"
)
efipkgs=(
	"efibootmgr"
)
strappkgs=(
	"git"
	"base-devel"
	"sddm"
)
aurpkgs=(
	"pamac-all"
	"snapd"
	"fossil"
	"mint-themes"
)
themes=(
	"oxygen-icons"
	"oxygen-icons-svg"
)
apps=(
	"blanket"
	"vlc"
	"nemo"
	"chromium"
	"flameshot"
	"celluloid"
	"libreoffice"
	"palemoon-gtk3-bin"
	"tilda"
	"terminator"
	"kweather"
	"kclock"
	"kamoso"
	"plasma-mobile-sounds"
)
desktop="cinnamon"
echo "user name?"
read user
echo bootstrapping...
pacstrap ./ ${bootpkgs[*]} ${strappkgs[*]}
echo configuring...
if [ $2 == "--efi" ]
then
	pacstrap ./ ${efipkgs[*]}
fi
arch-chroot ./ systemctl --user enable pipewire pipewire-pulse wireplumber
arch-chroot ./ systemctl enable dhcpcd NetworkManager sddm
echo installing...
pacstrap ./ $desktop
pacstrap ./ ${themes[*]}
echo "$user's password?"
arch-chroot ./ useradd -m $user
arch-chroot ./ passwd $user
echo "root password?"
arch-chroot ./ passwd root
arch-chroot ./ sh -c 'echo "'$user' ALL=(ALL:ALL) ALL" >> /etc/sudoers'
arch-chroot ./ sudo -s <<EOF
	sudo pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com
	sudo pacman-key --lsign-key 3056513887B78AEB
	sudo pacman -U --noconfirm 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst'
	sudo pacman -U --noconfirm 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
EOF
arch-chroot ./ sudo -u $user -s <<EOF
	cd ~
	mkdir .build
	cd .build
	git clone --depth=1 https://aur.archlinux.org/yay-bin.git
	cd yay-bin
	makepkg -si --noconfirm
	cd ..
	rm -rf yay-bin
	git clone --depth=1 https://codeberg.org/luckyshacky/wiggly.git
	cd wiggly
	cd pkgs/wallpapers/
	makepkg -si --noconfirm
	cd ..
	cd core
	makepkg -s
	sudo pacman -U --overwrite "*" *.tar.zst
	cd ~
	yay -S --noconfirm ${aurpkgs[*]}
	yay -S --noconfirm ${apps[*]}
EOF
if [ ! $? ]
then
	echo 'INSTALL FAILED!'
	exit 1
fi
echo finalizing...
if [ $2 == "--efi" ]
then
	mount -t efivarfs none /sys/firmware/efi/efivars
fi
arch-chroot ./ grub-install $1
arch-chroot ./ grub-mkconfig -o /boot/grub/grub.cfg
sync
sleep 5
echo "INSTALL COMPLETE!"
