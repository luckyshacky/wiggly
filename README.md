# wiggly

wiggly linux is a arch based distro made with the finest technologies.<br>
this distro will come with the best things like:<br>
- an app store (pamac)
- the chaotic aur (good repositories)
- snap (good package manager)
- yay (a better portage)
- cute wallpapers
- a good desktop that you can get used to (cinnamon)
- most sound firmware
- therapy apps (blanket,breathing,etc)
- good media players (celluloid,vlc,etc)
- good version control (fossil,git)
- good office software (libreoffice)
- amazing web browsers (palemoon,chromium)
- <br>
<br>
if it doesnt work right, let me know in the issues so i can fix the problem
